<?php
require_once 'FaceInterface.php';

class Face implements FaceInterface {

    private $id;
    private $race;
    private $emotion;
    private $oldness;

    /**
     * Creates a Face instance
     * @param int $race Race of the face. Should be from 0 to 100
     * @param int $emotion Emotion level of the face. Should be from 0 to 1000
     * @param int $oldness Oldness level of the face. Should be from 0 to 1000
     * @param int $id Id of the face. If id=0, the face is meant to be new
     * @throws Exception If any parameter is out of bounds
     */
    public function __construct(int $race, int $emotion, int $oldness, int $id = 0)
    {
        if ($race < 0 || $race > 100)
            throw new Exception('Race parameter is out of bounds');
        if ($emotion < 0 || $emotion > 1000)
            throw new Exception('Emotion level parameter is out of bounds');
        if ($oldness < 0 || $oldness > 1000)
            throw new Exception('Oldness level parameter is out of bounds');
            
        $this->id = $id;
        $this->race = $race;
        $this->emotion = $emotion;
        $this->oldness = $oldness;
    }

    /**
     * Returns face id or 0, if face is new
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Returns race parameter: from 0 to 100.
     */
    public function getRace(): int
    {
        return $this->race;
    }

    /**
     * Returns face emotion level: from 0 to 1000.
     */
    public function getEmotion(): int
    {
        return $this->emotion;
    }

    /**
     * Returns face oldness level: from 0 to 1000.
     */
    public function getOldness(): int
    {
        return $this->oldness;
    }
}
