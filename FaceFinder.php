<?php
require_once 'FaceFinderInterface.php';
require_once 'FaceInterface.php';
require_once 'Face.php';

class FaceFinder implements FaceFinderInterface {

    /**
     * @var string Table name all faces are stored in
     */
    const DB_NAME = 'face_finder';

    /**
     * @var string Table name all faces are stored in
     */
    const FACES_TABLE = 'faces';

    /**
     * @var int How many rows of FACES_TABLE are set for searching
     */
    const RESOLVE_NUM = 10000;

    /**
     * @var int How many rows should resolve function return
     */
    const RESOLVE_COUNT = 5;

    /**
     * @var int Maximum of race level
     */
    const RACE_MAX = 100;

    /**
     * @var int Maximum of emotion level
     */
    const EMOTION_MAX = 1000;

    /**
     * @var int Maximum of oldness level
     */
    const OLDNESS_MAX = 1000;

    /**
     * @var int By how many parts should we divide the space
     */
    const GRID_DIVIDER = 60;

    /**
     * @var array Should have grid divider for every axis. In our case it should be [6, 60, 60]
     * But we should calculate it with RACE_MAX, EMOTION_MAX, OLDNESS_MAX and GRID_DIVIDER constants in __construct
     */
    private $grid_divider_axis;

    /**
     * @var \mysqli Database connection handle
     */
    private $dbh;

    /**
     * @var array of \mysqli_stmt Prepared SQL-statements for getting last ids, anything else
     */
    private $prepared_stmts;

    /**
     * @var array of Face. Should contain last RESOLVE_NUM of FACES_TABLE. Keys are ids
     */
    private $cache = [];

    /**
     * @var array List of face_ids, identified by its coordinates in the grid
     * Ex: $grid[$race_coord][$emotion_coord][$oldness_coord] = [
     *      $face_id_1 => 1,
     *      $face_id_2 => 1,
     *      ...
     *     ];
     * Face_ids are keys in the fourth dimension for the purposes of uniqueness
     */
    private $grid = [];

    /**
     * This two variables are used to check cache validity
     * 
     * @var int $max_id - is max face_id of FACES_TABLE
     * @var int $min_id - is face_id of RESOLVE_NUM's row of FACES_TABLE
     */
    private $max_id = 0;
    private $min_id = 0;

    /**
     * Creates a FaceFinder instance
     * Creates a database structure to store faces as well
     * 
     * @throws Exception On database connection or query failure
     */
    public function __construct()
    {
        // Read a config file with db connection parameters
        $config = parse_ini_file('config.ini');

        // Connection to db
        $this->dbh = new \mysqli(
            'p:'.$config['DB_HOST'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            '',
            !empty($config['DB_PORT'])?$config['DB_PORT']:3306
        );
        if ($this->dbh->connect_error)
            throw new Exception($this->dbh->connect_error);

        // Create a database if it does not exist yet
        $sql = "
            create database if not exists ".self::DB_NAME."
        ";
        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        // Select database for all queries
        $this->dbh->select_db(self::DB_NAME);

        // Create a table if it does not exist yet
        $sql = "
            create table if not exists ".self::FACES_TABLE." (
                face_id int(11) not null auto_increment,
                race tinyint(4) not null,
                emotion smallint(6) not null,
                oldness smallint(6) not null,
                primary key (face_id)
            ) engine=InnoDB
        ";
        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        // Create a table of ids. max_id - max(faces.face_id), min_id - 10000th face_id, or 0 if table has less than 10000 rows
        // It will have just one row. Filled in by trigger
        $sql = "
            create table if not exists ".self::FACES_TABLE."_ids (
                id int(11) not null,
                max_id int(11) not null,
                min_id int(11) not null,
                primary key (id)
            ) engine=InnoDB
        ";
        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        // Create a table of ids deleted from faces. Filled in by trigger
        $sql = "
            create table if not exists ".self::FACES_TABLE."_deletes (
                face_id int(11) not null,
                primary key (face_id)
            ) engine=InnoDB
        ";
        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        // Create trigger that would fill max and min ids
        $sql = "
            create trigger ".self::FACES_TABLE."_after_insert after insert on ".self::FACES_TABLE."
            for each row
                update
                    ".self::FACES_TABLE."_ids
                set
                    max_id = new.face_id,
                    min_id = (select face_id from ".self::FACES_TABLE." order by face_id desc limit ".(self::RESOLVE_NUM - 1).", 1)
                where
                    id = 1
        ";
        // This trigger may already exist. Thats not an error.
        // If only "create trigger" operation had "if not exists" option
        @($this->dbh->query($sql));

        // Create trigger that will contain max and min ids
        $sql = "
            create trigger ".self::FACES_TABLE."_after_delete after delete on ".self::FACES_TABLE."
            for each row
            begin
                insert into ".self::FACES_TABLE."_deletes
                (
                    face_id
                )
                values
                (
                    old.face_id
                )
                ;

                update
                    ".self::FACES_TABLE."_ids
                set
                    max_id = (select max(face_id) from ".self::FACES_TABLE."),
                    min_id = (select face_id from ".self::FACES_TABLE." order by face_id desc limit ".(self::RESOLVE_NUM - 1).", 1)
                where
                    id = 1
                ;
            end;
        ";
        // This trigger may already exist. Thats not an error.
        // If only "create trigger" operation had "if not exists" option
        @($this->dbh->query($sql));

        // Insert primary data into faces_ids
        $sql = "
            insert ignore into ".self::FACES_TABLE."_ids
            (
                id,
                max_id,
                min_id
            )
            values
            (
                1,
                0,
                0
            )
        ";
        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        // Calculate grid_divider_axis
        $coords_max = max(self::RACE_MAX, self::EMOTION_MAX, self::OLDNESS_MAX);
        $this->grid_divider_axis = [
            (int)floor(self::RACE_MAX / $coords_max * self::GRID_DIVIDER),
            (int)floor(self::EMOTION_MAX / $coords_max * self::GRID_DIVIDER),
            (int)floor(self::OLDNESS_MAX / $coords_max * self::GRID_DIVIDER),
        ];

        // Make cache
        $this->validateCache();
    }

    /**
     * Adds a face to cache and grid
     * 
     * @param FaceInterface $face A face to add
     */
    private function addToCache(FaceInterface $face): void
    {
        $face_id = $face->getId();
        // Add to cache
        $this->cache[$face_id] = $face;

        // Add to grid
        $coords = $this->getGridCoords($face);
        $this->grid[$coords[0]][$coords[1]][$coords[2]][$face_id] = 1;
    }

    /**
     * Deletes a face from cache and grid
     * 
     * @param int $face_id Id of the face
     */
    private function deleteFromCache(int $face_id): void
    {
        if (!isset($this->cache[$face_id]))
            return;

        // Remove from grid
        $coords = $this->getGridCoords($this->cache[$face_id]);
        unset($this->grid[$coords[0]][$coords[1]][$coords[2]][$face_id]);

        // Remove from cache
        unset($this->cache[$face_id]);
    }

    /**
     * Saves a face in DB
     * 
     * @param FaceInterface $face A face to save
     * @throws Exception On database query failure
     */
    private function saveFace(FaceInterface $face): void
    {
        // Face parameters
        // There is a need to save them as variables before binding to query params
        $race = $face->getRace();
        $emotion = $face->getEmotion();
        $oldness = $face->getOldness();

        // Insering a new face to the db if needed
        $sql = "
            insert into ".self::FACES_TABLE."
            (
                race,
                emotion,
                oldness
            )
            values
            (
                ?,
                ?,
                ?
            )
        ";
        
        $stmt = $this->dbh->prepare($sql);
        if (
            !$stmt ||
            !$stmt->bind_param('iii', $race, $emotion, $oldness) ||
            !$stmt->execute()
        )
            throw new Exception($this->dbh->error);

        $new_face_id = $this->dbh->insert_id;
        // If nothing was inserted since last insert by this very class, we can just cache it on application side (maybe) without revalidation by database
        if ($new_face_id == $this->max_id + 1) {
            $this->addToCache(new Face($race, $emotion, $oldness, $new_face_id));
            $this->max_id++;
            // It's too hard to know wether we should increment min_id from 0 to 1 or not. So I go the easiest way
            // The bad thing about it is if we cross the line from RESOLVE_NUM-1 to RESOLVE_NUM rows, we are going revalidation even if we shouldn't
            // But I'm ok with it :)
            if ($this->min_id != 0)
                $this->min_id++;
        }
    }

    /**
     * Gets max_id and min_id of FACES_TABLE saved in FACES_TABLE_ids table
     * Saves them to $this->max_id and $this->min_id
     * 
     * @return bool Were max_id or min_id changed? If so, we should do revalidation of the cache
     * @throws Exception On database query failure and having no data in FACES_TABLE_ids
     */
    private function isRevalidationNeeded(): bool
    {
        // If search statement is not prepared yet - prepare it
        if (empty($this->prepared_stmts['last_faces_ids'])) {
            $sql = "
                select
                    max_id,
                    min_id
                from
                    ".self::FACES_TABLE."_ids
                where
                    id = 1
            ";
            $this->prepared_stmts['last_faces_ids'] = $this->dbh->prepare($sql);
            if (!$this->prepared_stmts['last_faces_ids'])
                throw new Exception($this->dbh->error);
        }

        // Executing search statement with face parameters
        if (
            !$this->prepared_stmts['last_faces_ids']->execute() ||
            !$this->prepared_stmts['last_faces_ids']->bind_result($max_id, $min_id) ||
            !$this->prepared_stmts['last_faces_ids']->store_result()
        )
            throw new Exception($this->dbh->error);

        $result = false;
        if ($this->prepared_stmts['last_faces_ids']->fetch()) {
            // If max_id or min_id changed then revalidation is truely needed
            if ($this->max_id != $max_id || $this->min_id != $min_id)
                $result = true;
            $this->max_id = $max_id;
            $this->min_id = $min_id;
        }
        else
            throw new Exception('No data in IDS table');

        return $result;
    }

    /**
     * Gets deleted faces in the range of last RESOLVE_NUM rows and deletes them form cache
     * 
     * @uses $this->max_id, $this->min_id Using them because we don't want to get race condition
     * @throws Exception On database query failure
     */
    private function uncacheDeletedFaces(): void
    {

        $sql = "
            select
                d.face_id
            from
                ".self::FACES_TABLE."_deletes d
            where
                d.face_id <= ? and
                d.face_id >= ? and
                ".(empty($this->cache)?'1':('d.face_id in ('.implode(', ', array_keys($this->cache)).')'))."
            order by
                d.face_id
        ";

        // There's no need to save this prepared statement
        $stmt = $this->dbh->prepare($sql);

        if (
            !$stmt ||
            !$stmt->bind_param('ii', $this->max_id, $this->min_id) ||
            !$stmt->execute() ||
            !$stmt->bind_result($res_id)
        )
            throw new Exception($this->dbh->error);

        while ($stmt->fetch())
            $this->deleteFromCache($res_id);
    }

    /**
     * Gets new faces that are not in the cache and caches them
     * 
     * @uses $this->max_id, $this->min_id Using them because we don't want to get race condition
     * @throws Exception On database query failure
     */
    private function cacheUncachedFaces(): void
    {
        $sql = "
            select
                f.face_id,
                f.race,
                f.emotion,
                f.oldness
            from
                ".self::FACES_TABLE." f
            where
                f.face_id <= ? and
                f.face_id >= ? and
                ".(empty($this->cache)?'1':('f.face_id not in ('.implode(', ', array_keys($this->cache)).')'))."
            order by
                f.face_id
        ";

        // There's no need to save this prepared statement
        $stmt = $this->dbh->prepare($sql);

        if (
            !$stmt ||
            !$stmt->bind_param('ii', $this->max_id, $this->min_id) ||
            !$stmt->execute() ||
            !$stmt->bind_result($res_id, $res_race, $res_emotion, $res_oldness)
        )
            throw new Exception($this->dbh->error);

        while ($stmt->fetch())
            $this->addToCache(new Face($res_race, $res_emotion, $res_oldness, $res_id));
    }

    /**
     * Validates cache
     */
    private function validateCache(): void
    {
        $revalidation_needed = $this->isRevalidationNeeded();

        // If something was deleted and FACES_TABLE has less than RESOLVE_NUM rows (i.e. $this->min_id == 0)
        // then it will have no effect on $this->min_id. That means we should check for deletions anyway
        if ($this->min_id == 0 || $revalidation_needed) {
            $this->uncacheDeletedFaces();
        }
        // Cache new faces
        if ($revalidation_needed) {
            $this->cacheUncachedFaces();
        }
        // Delete all the faces that are out of RESOLVE_NUM bound, i.e. their face_id is less than $this->min_id
        foreach (array_keys($this->cache) as $face_id) {
            if ($face_id < $this->min_id)
                $this->deleteFromCache($face_id);
        }
    }

    /**
     * Gets one coordinate of the grid
     * 
     * @param int $value Current value of the parameter (i.e. race, emotion or oldness)
     * @param int $axis Axis. 0, 1 or 2. 0 for race, 1 for emotion, 2 for oldness
     * @return int Coordinate value. From 0 to grid_divider_axis[$coord]-1
     */
    private function getCoord(int $value, int $axis): int
    {
        if ($axis == 0)
            $max_value = self::RACE_MAX;
        elseif ($axis == 1)
            $max_value = self::EMOTION_MAX;
        elseif ($axis == 2)
            $max_value = self::OLDNESS_MAX;

        $result = (int)floor($value / $max_value * $this->grid_divider_axis[$axis]);
        if ($result < 0)
            $result = 0;
        if ($result >= $this->grid_divider_axis[$axis])
            $result = $this->grid_divider_axis[$axis] - 1;
        return $result;
    }

    /**
     * Gets the coordinates of the face on the grid
     * It does NOT saves the face to the $this->grid. Use addToCache for this purposes
     * 
     * @param FaceInterface $face The face we want get coordinates
     * @return array 3 coordinates on the grid. They stand for race, emotion or oldness
     */
    private function getGridCoords(FaceInterface $face): array
    {
        return [
            $this->getCoord($face->getRace(), 0),
            $this->getCoord($face->getEmotion(), 1),
            $this->getCoord($face->getOldness(), 2),
        ];
    }


    /**
     * Finds 5 most similar faces in DB.
     * If the specified face is new (id=0),
     * then it will be saved to DB.
     *
     * @param FaceInterface $face Face to find and save (if id=0)
     * @return FaceInterface[] List of 5 most similar faces,
     * including the searched one
     * @throws Exception On database query failure
     */
    public function resolve(FaceInterface $face): array
    {
        // Insering a new face to the db if needed
        if ($face->getId() == 0)
            $this->saveFace($face);

        // Make sure the cache is valid
        $this->validateCache();

        // We don't want to check all the cache. So we should determine the candidates
        $candidates = [];
        if (count($this->cache) <= 50)
            $candidates = $this->cache;
        else {
            // Make search on the grid
            $coords = $this->getGridCoords($face);
            $candidate_ids = [];
            $checked_blocks = []; // Array of blocks we already checked
            $step_max = 0;
            for ($step = 0; $step <= self::GRID_DIVIDER - 1; $step++) {
                for ($c0 = max($coords[0] - $step, 0); $c0 <= min($coords[0] + $step, $this->grid_divider_axis[0] - 1); $c0++) {
                    for ($c1 = max($coords[1] - $step, 0); $c1 <= min($coords[1] + $step, $this->grid_divider_axis[1] - 1); $c1++) {
                        for ($c2 = max($coords[2] - $step, 0); $c2 <= min($coords[2] + $step, $this->grid_divider_axis[2] - 1); $c2++) {
                            if (!empty($this->grid[$c0][$c1][$c2])) {
                                if (!empty($checked_blocks[$c0][$c1][$c2]))
                                    continue;
                                $checked_blocks[$c0][$c1][$c2] = 1;

                                array_push($candidate_ids, ...array_keys($this->grid[$c0][$c1][$c2]));
                            }
                        }
                    }
                }

                // If we already found what we was looking for, let's calculate which step we should reach
                if ($step_max == 0 && count($candidate_ids) >= self::RESOLVE_COUNT)
                    $step_max = (int)ceil(sqrt(3) * ($step + 1));

                // If we've reached the maximum step needed, just exit this cycle
                if ($step_max != 0 && $step >= $step_max)
                    break;
            }

            foreach ($candidate_ids as $face_id) {
                $candidates[$face_id] = $this->cache[$face_id];
            }
        }

        // Calculate the distances from our face to all the candidates
        $face_race = $face->getRace();
        $face_emotion = $face->getEmotion();
        $face_oldness = $face->getOldness();

        $distances = [];
        foreach ($candidates as $candidate) {
            $distances[$candidate->getId()] = 
                ($face_race - $candidate->getRace()) ** 2 +
                ($face_emotion - $candidate->getEmotion()) ** 2 +
                ($face_oldness - $candidate->getOldness()) ** 2
            ;
        }
        // Sort distances. Preserving keys. Keys are the most valuable
        asort($distances);

        // Make result
        $result = [];
        foreach (array_slice(array_keys($distances), 0, self::RESOLVE_COUNT) as $face_id) {
            $result[] = $this->cache[$face_id];
        }

        return $result;
    }

    /**
     * Removes all faces in DB and (!) reset faces id sequence
     * 
     * @throws Exception On database query failure
     */
    public function flush(): void
    {
        $sql = "
            truncate table ".self::FACES_TABLE."
        ";
        
        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        $sql = "
            truncate table ".self::FACES_TABLE."_deletes
        ";
        
        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        $sql = "
            update
                ".self::FACES_TABLE."_ids
            set
                max_id = 0,
                min_id = 0
            where
                id = 1
        ";

        if (!$this->dbh->query($sql))
            throw new Exception($this->dbh->error);

        $this->cache = [];
        $this->grid = [];
        $this->max_id = 0;
        $this->min_id = 0;
    }
}
