<?php
require_once 'Face.php';

function connect()
{
    static $dbh;

    if (empty($dbh)) {
        $config = parse_ini_file('config.ini');

        // Connection to db
        $dbh = new \mysqli(
            'p:'.$config['DB_HOST'],
            $config['DB_USERNAME'],
            $config['DB_PASSWORD'],
            'face_finder',
            !empty($config['DB_PORT'])?$config['DB_PORT']:3306
        );
    }

    return $dbh;
}

function face_find_db(FaceInterface $face): array
{
    static $stmt;

    $dbh = connect();

    if (empty($stmt)) {
        $sql = "
            select
                f.face_id,
                f.race,
                f.emotion,
                f.oldness,
                pow(f.race - ?, 2) + pow(f.emotion - ?, 2) + pow(f.oldness - ?, 2) dist
            from
                faces f
            inner join
                faces_ids fi
                on
                    fi.id = 1 and
                    f.face_id <= fi.max_id and
                    f.face_id >= fi.min_id
            order by
                dist
            limit
                5
        ";

        $stmt = $dbh->prepare($sql);
        if (!$stmt)
            throw new Exception($dbh->error);
    }

    $race = $face->getRace();
    $emotion = $face->getEmotion();
    $oldness = $face->getOldness();

    if (
        !$stmt->bind_param('iii', $race, $emotion, $oldness) ||
        !$stmt->execute() ||
        !$stmt->bind_result($res_face_id, $res_race, $res_emotion, $res_oldness, $dist)
    )
        throw new Exception($dbh->error);

    $result = [];
    while ($stmt->fetch())
        $result[] = new Face($res_race, $res_emotion, $res_oldness, $res_face_id);

    return $result;
}


function insert_some_face(): void
{
    $dbh = connect();

    $p1 = rand(0, 100);
    $p2 = rand(0, 1000);
    $p3 = rand(0, 1000);

    $sql = "
        insert into faces
        (
            race,
            emotion,
            oldness
        )
        values
        (
            ".$p1.",
            ".$p2.",
            ".$p3."
        )
    ";

    if (!$dbh->query($sql))
        throw new Exception($dbh->error);
}


function delete_some_face(): void
{
    $dbh = connect();

    $sql = "
        select
            max_id
        from
            faces_ids
        where
            id = 1
    ";
    $stmt = $dbh->prepare($sql);

    if (
        !$stmt ||
        !$stmt->execute() ||
        !$stmt->bind_result($max_id) ||
        !$stmt->store_result()
    )
        throw new Exception($dbh->error);

    $stmt->fetch();

    $face_id = floor($max_id * mt_rand() / mt_getrandmax());

    $sql = "
        delete from
            faces
        where
            face_id = ".$face_id."
    ";

    if (!$dbh->query($sql))
        throw new Exception($dbh->error);
}
