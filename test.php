<?php
require_once 'FaceFinder.php';
require_once 'face_finder_db.php';

function compareFaces(FaceInterface $f1, FaceInterface $f2, FaceInterface $face): bool
{
    if ($f1->getId() == $f2->getId())
        return true;
    if (
        ($face->getRace() - $f1->getRace()) ** 2 +
        ($face->getEmotion() - $f1->getEmotion()) ** 2 +
        ($face->getOldness() - $f1->getOldness()) ** 2
        ==
        ($face->getRace() - $f2->getRace()) ** 2 +
        ($face->getEmotion() - $f2->getEmotion()) ** 2 +
        ($face->getOldness() - $f2->getOldness()) ** 2
    )
        return true;

    return false;
}

$ff = new FaceFinder();

$resolve_total_time = 0;
$db_total_time = 0;

for ($i = 0; $i < 100000; $i++) {
    $face = new Face(rand(0, 100), rand(0, 1000), rand(0, 1000), 1);

    $time_start = microtime(true);
    $my_res = $ff->resolve($face);
    $time_end = microtime(true);
    $resolve_total_time += $time_end - $time_start;

    $time_start = microtime(true);
    $db_res = face_find_db($face);
    $time_end = microtime(true);
    $db_total_time += $time_end - $time_start;

    foreach ($db_res as $k => $res_face) {
        if (!compareFaces($res_face, $my_res[$k], $face)) {
            var_dump($face, $db_res, $my_res);
            die;
        }
    }

    if ($i % 100 == 0) {
        insert_some_face();
        insert_some_face();
        delete_some_face();
    }

    if ($i % 50 == 0) {
        echo($i."\t".$resolve_total_time."\t".$db_total_time."\n");
    }
}
